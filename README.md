# PID Envelope Generator JSFX

JesuSonic FX (JSFX) implementations of the [PID Envelope Generator (PIDEG)](https://arxiv.org/abs/2106.13966) specification. JSFX scripts can be used for music production in Cockos REAPER & other DAWs.

This repository consists of:

1. Library that implements core PIDEG functionalities. It can be integrated into any other JSFX script.
2. A demo plugin that employes the PIDEG library and manipulates incoming MIDI message velocities as per the input parameter values provided by the user. The result is then passed to the succeeding modules of that track.

## Installation

**Linux:**

1. Clone this repo to local

```sh
git clone https://gitlab.com/pid-envelope/pideg-jsfx.git
```

2. Create a soft simlink of the scripts to Reaper Effects directory

```sh
ln -s "<local_path_to_this_repo>/PIDEG_lib.jsfx-inc" "~/.config/REAPER/Effects/"
ln -s "<local_path_to_this_repo>/PIDEG.jsfx" "~/.config/REAPER/Effects/"
```

## Usage

### Using the library in another JSFX script

#### Prerequisite

The script must define the following variables beforehand (ideally, provide them as slider inputs):

- Kp => Proportional Component Gain Factor
- Ki => Integral Component Gain Factor
- Kd => Derivative Component Gain Factor
- Kr => Rise Factor [on NOTE-ON]
- Kf => Fall Factor [on NOTE-OFF]

1. Importing the library

```java
import PIDEG_lib.jsfx-inc
```

2. Using the library

```java
<object_name>.pideg_process()
//example: pideg.pideg_process()
```

### Using the demo plugin in a REAPER project

1. Post installation, the plugin should appear in the FX List as `MIDI_PIDEG.jsfx`
2. Add the plugin to any FX chain. **Important: This plugin must either be the first one in the chain or should only be preceeded by midi-to-midi processors.**
3. Set/Modulate any of the provided input sliders to control PIDEG behaviour as desired.

![](doc/demo_plugin_fx_track.png)

#### Note

The plugin internally calculates:

1. Log10 values of Kp, Ki and Kd slider inputs to restrict their range to [0-2] and focus on the region where changes are audible.
2. 10th power of Kr and Kf to support the huge range of [0.01-100000] without comprimising on granularity of control.

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/pid-envelope/pideg-jsfx/-/issues)
1. Fork it (<https://gitlab.com/pid-envelope/pideg-jsfx>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the Mozilla Public License Version 2.0 License. See `LICENSE` for more information.
