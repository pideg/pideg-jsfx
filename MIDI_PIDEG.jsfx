/*
SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
*/

desc: MIDI PIDEG v1.0
Shapes incoming MIDI message velocities as per the PIDEG specification.

Brief description of the input parameters:
Kp => Proportional Component Gain Factor [log10 variable]
Ki => Integral Component Gain Factor [log10 variable]
Kd => Derivative Component Gain Factor [log10 variable]
Kr => Rise Factor [on NOTE-ON] [10^ variable]
Kf => Fall Factor [on NOTE-OFF] [10^ variable]

import PIDEG_lib.jsfx-inc

//PIDEG
slider1:slide_Kp=1<1,100,0.01>Kp
slider2:slide_Ki=1<1,100,0.01>Ki
slider3:slide_Kd=1<1,100,0.01>Kd 
slider4:slide_Kr=0<-2,5,0.01>Kr
slider5:slide_Kf=0<-2,5,0.01>Kf

in_pin:left input
in_pin:right input
out_pin:left output
out_pin:right output

@init 

//PIDEG
pideg.Kp=log10(slide_Kp);
pideg.Ki=log10(slide_Ki);
pideg.Kd=log10(slide_Kd);
pideg.Kr=10^(slide_Kr);
pideg.Kf=10^(slide_Kf);

pideg.initialize();

//MIDI
noteOn = $x90; // note statusbyte

@slider
pideg.Kp=log10(slide_Kp);
pideg.Ki=10^(slide_Ki);
pideg.Kd=log10(slide_Kd);
pideg.Kr=10^(slide_Kr);
pideg.Kf=10^(slide_Kf);

@sample
while (midirecv(offset,msg1,msg2,msg3)) (
  og_m1 = msg1;
  note_status = msg1 & $xF0;
  channel = msg1 & $x0F;
  current_note = msg2;

  note_status==noteOn && pideg.mode != 1 ? (
    pideg.initialize();
    pideg.key_on(); 
  ) : pideg.mode != 0 ? (
    pideg.key_off();
  );
);

pideg_output = pideg.pideg_process() * msg3;
midisend(offset,og_m1,msg2,pideg_output);
